package proj.dyno;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by mbarcelona on 3/8/16.
 */
public class CanvasRPMView extends View {

  public float needleX, needleY, needleHeight, needleWidth;


  private Paint linePaint;
  private Path linePath;

  private Matrix matrix;
  private int framePerSeconds = 100;
  private long animationDuration = 10000;
  private long startTime;

  public CanvasRPMView(Context context) {
    super(context);
    matrix = new Matrix();
    this.startTime = System.currentTimeMillis();
    this.postInvalidate();
    init();
  }

  public CanvasRPMView(Context context, AttributeSet attrs) {
    super(context, attrs);
    matrix = new Matrix();
    this.startTime = System.currentTimeMillis();
    this.postInvalidate();
    init();
  }

  public CanvasRPMView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    matrix = new Matrix();
    this.startTime = System.currentTimeMillis();
    this.postInvalidate();
    init();
  }

  private void init() {

    //needle
    needleX = 510;//this.getWidth()/2;
    needleY = 600;//this.getHeight()/3;
    needleHeight = 400;
    needleWidth = 5;

    linePaint = new Paint();
    linePaint.setColor(Color.RED); // Set the color
    linePaint.setStyle(Paint.Style.FILL_AND_STROKE); // set the border and fills the inside of needle
    linePaint.setAntiAlias(true);
    linePaint.setStrokeWidth(2.0f); // width of the border
    //linePaint.setShadowLayer(8.0f, 0.1f, 0.1f, Color.GRAY); // Shadow of the needle

    linePath = new Path();
    linePath.moveTo(needleX, needleY);
    linePath.lineTo(needleX+20.0f, needleY-needleWidth);
    linePath.lineTo(needleX+needleHeight,needleY);
    linePath.lineTo(needleX+20.0f, needleY+needleWidth);
    linePath.lineTo(needleX,needleY);
    linePath.addCircle(needleX+40.0f, needleY, 10.0f, Path.Direction.CW);
    linePath.close();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);


    long elapsedTime = System.currentTimeMillis() - startTime;

    matrix.postRotate(1.0f, needleX+40.0f, needleY); // rotate 10 degree every second

    canvas.drawPath(linePath, linePaint);

    //canvas.drawCircle(130.0f, 50.0f, 16.0f, needleScrewPaint);

    if(elapsedTime < animationDuration){
      this.postInvalidateDelayed(10000 / framePerSeconds);
    }

    //this.postInvalidateOnAnimation();
    invalidate();
  }
}