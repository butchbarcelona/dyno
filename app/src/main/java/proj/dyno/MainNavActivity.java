package proj.dyno;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import proj.dyno.blunobasicdemo.BlunoLibrary;
import proj.dyno.objects.Dashboard;
import proj.dyno.views.GraphFragment;

public class MainNavActivity extends BlunoLibrary
        implements NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "DYNO";

    GraphFragment graphFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_nav);

        onCreateProcess();
        serialBegin( 115200 );

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        graphFragment = new GraphFragment();
        //goToFragment(R.id.nav_graph);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        goToFragment(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goToFragment(int id){
        // Create new fragment and transaction
        Fragment newFragment = null;

        switch(id){
            case R.id.nav_rpm:
                graphFragment.setFragType(Dashboard.FRAG_RPM);
                newFragment = graphFragment;
                break;
            case R.id.nav_graph:
                graphFragment.setFragType(Dashboard.FRAG_GRAPH);
                newFragment = graphFragment;
                break;
        }


        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, newFragment);
        //transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }


    protected void onResume() {
        super.onResume();
        onResumeProcess();                                                        //onResume Process by BlunoLibrary
    }
    @Override
    protected void onPause() {
        super.onPause();
        onPauseProcess();                                                        //onPause Process by BlunoLibrary
    }

    protected void onStop() {
        super.onStop();
        onStopProcess();                                                        //onStop Process by BlunoLibrary
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDestroyProcess();                                                        //onDestroy Process by BlunoLibrary
    }

    @Override
    public void onConectionStateChange(
      connectionStateEnum theConnectionState ) {//Once connection state changes, this function will be called
        switch( theConnectionState ) {                                            //Four connection state
            case isConnected:
                Log.d(TAG,"Connected");
                break;
            case isConnecting:
                Log.d(TAG, "Connecting");
                break;
            case isToScan:
                Log.d(TAG, "Scan");
                break;
            case isScanning:
                Log.d(TAG, "Scanning");
                break;
            case isDisconnecting:
                Log.d(TAG, "isDisconnecting");
                break;
            default:
                break;
        }
    }

    @Override
    public void onSerialReceived( String bleData ) {
//        if( mStringBuffer == null ) {
//            mStringBuffer = new StringBuffer();
//        }
//        mStringBuffer.append( bleData );
//        if( bleData.contains( "@" ) ) {
//            String data = mStringBuffer.toString();
//            data = data.replace( "(\\r|\\n)", "" );
//            String dataArr[] = data.split( "," );
//            for( int i = 1; i < dataArr.length - 1; i++ ) {
//                Log.e( "Bluno", dataArr[ i ] );
//            }
//            mStringBuffer = null;
//        }
    }
}
