package proj.dyno.objects;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.Typeface;
import android.util.Log;

public class Dashboard {

	final int DRAWDASHBOARD = 0;
	final int UPDATEDASHBOARD = 1;
	private int state; // = DRAWDASHBOARD;


	public static final int FRAG_GRAPH = 0x02;
	public static final int FRAG_RPM = 0x03;
	public int fragType = FRAG_GRAPH;


	protected static final float fullAccel = 12.0f; // miles / sec / sec
	protected static final float fullDecel = 50.0f; // miles/s/s when foot is on the brake
	protected static final float minDecel  = 1.0f;  // miles/s/s when foot is off the gas

	protected static final int ODOMETER_CENTER_X_PERCENT_PORT = 50;
	protected static final int ODOMETER_CENTER_Y_PERCENT_PORT = 60;
	protected static final double ODOMETER_RADIUS_PERCENT_PORT = 30.0;

	protected static final float idleRPM = 1500;
	protected static final int TACHOMETER_CENTER_X_PERCENT_PORT = 50;
	protected static final int TACHOMETER_CENTER_Y_PERCENT_PORT = 60;
	protected static final double TACHOMETER_RADIUS_PERCENT_PORT = 30.0;


	protected static float mph = 0.1f;
	protected static float rpm = 0;


	//Button gasButton;
	protected static boolean gasPedalOn = false;
	protected static boolean brakePedalOn = false;
	protected static boolean engineOn = false;

	protected Point odometerCenter;
	protected Point tachometerCenter;
/*	protected Point fuelGaugeCenter;
	protected Point tempGaugeCenter;*/

	public Paint needlePaint;

	// reading for this screen:


	GaugeView.GaugeThread mThread;
	public Dashboard(GaugeView.GaugeThread thread, int fragType)
	{

		this.fragType = fragType;
		mThread = thread;

		setFragType(this.fragType);

    	state = DRAWDASHBOARD;
		needlePaint = new Paint();
		needlePaint.setStrokeWidth(2);
		needlePaint.setColor(Color.RED);

		if(mThread.paint == null){
			mThread.paint = new Paint();
		}

	}

	public void setFragType(int fragType){
		this.fragType = fragType;
		state = DRAWDASHBOARD;
		int a_x, a_y ;

		if(fragType == FRAG_GRAPH) {
			a_x = (mThread.mCanvasWidth * Dashboard.ODOMETER_CENTER_X_PERCENT_PORT) / 100;
			a_y = (mThread.mCanvasHeight * Dashboard.ODOMETER_CENTER_Y_PERCENT_PORT) / 100;
			odometerCenter = new Point(a_x, a_y);
		}else
		if(fragType == FRAG_RPM) {
			a_x = (mThread.mCanvasWidth * Dashboard.TACHOMETER_CENTER_X_PERCENT_PORT) / 100;
			a_y = (mThread.mCanvasHeight * Dashboard.TACHOMETER_CENTER_Y_PERCENT_PORT) / 100;
			tachometerCenter = new Point(a_x, a_y);
		}
	}
	protected void dashBoardState(Canvas canvas, Context mContext)
	{
		//drawDashBoard(canvas, mContext);

		switch (state)
		{
		case DRAWDASHBOARD:
			//drawDashBoard(c);
			drawDashBoard(canvas, mContext);
			state = UPDATEDASHBOARD;
			break;
		case UPDATEDASHBOARD:
			updatePhysics();
			doDraw(canvas);
		}

	}
	void drawDashBoard(Canvas canvas, Context mContext)
	{

		Log.d("TONS","do draw first time");
		mThread.background = new Picture();
		canvas = mThread.background.beginRecording(mThread.mCanvasWidth, mThread.mCanvasHeight);
		// background:
		//Rect r = new Rect(0, 0, mCanvasWidth, mCanvasHeight);
		Paint p = new Paint();
		p.setColor(Color.BLACK);

		canvas.drawARGB(255, 0, 0, 0);

		Typeface typeface;
		typeface = Typeface.create("sans", Typeface.BOLD);

		p.setAntiAlias(true);

		p.setTextSize(30);
		p.setTypeface(typeface);

		if(fragType == FRAG_GRAPH) {
			mThread.odo = new Odometer();
			mThread.odo.drawOdometer(canvas,
				p,
				odometerCenter,
				(double) mThread.mCanvasWidth * ODOMETER_RADIUS_PERCENT_PORT / 100.0,
				Math.PI, -Math.PI / 4.0,
				0, 300,
				16);
		}else{
			mThread.tach = new Tachnometer();
			mThread.tach.drawTachnometer(canvas,
				p,
				tachometerCenter,
				(double) mThread.mCanvasWidth * TACHOMETER_RADIUS_PERCENT_PORT / 100.0,
				Math.PI, Math.PI / 4.0,
				0, 7000,
				8);
		}


		mThread.background.endRecording();

		Log.d("TONS", "do draw first time end");
	}

	public void setMPH(float mph){
		this.mph = mph;
	}

	public void setRPM(float rpm){
		this.rpm = rpm;
	}


	public void updatePhysics()
	{

		if ((gasPedalOn) //&& (engineOn)
			)
		{
			mph += fullAccel / 2.0;
			rpm += 100;
		}
		else
		{
			mph -= minDecel / 2.0;
		}
		if (brakePedalOn)
		{
			mph -= fullDecel / 2.0;
		}

		// do not let speed go past 150 or less than zero:
		mph = (mph > 150.0)?150.0f:mph;
		mph = (mph < 0.0)?0.0f:mph;



	}
	protected void doDraw(Canvas canvas) {

		mThread.background.draw(canvas);
		mThread.paint.setTextSize(60);
		mThread.paint.setColor(Color.CYAN);


		if(fragType == FRAG_GRAPH) {
			int mphLocX_txt = odometerCenter.x - mThread.percentWidth(5);
			int mphLocY_txt = odometerCenter.y + mThread.percentHeight(3);
			Log.d("TONS", "mph:" + mph);

			canvas.drawText(mThread.get3digit(mph), mphLocX_txt, mphLocY_txt, mThread.paint);
			mThread.odo.drawNeedle(canvas, (double) mph, needlePaint);
		}else {
			// Tachometer:
			int rpmLocX_txt = tachometerCenter.x - mThread.percentWidth(5);
			int rpmLocY_txt = tachometerCenter.y + mThread.percentHeight(3);
			canvas.drawText(mThread.get4digit(rpm), rpmLocX_txt, rpmLocY_txt, mThread.paint);
			mThread.tach.drawNeedle(canvas, (double) rpm, needlePaint);
			Log.d("TONS", "rpm:" + rpm);
		}



	}

}
