package proj.dyno.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

public class Gauge {
	
	public double minValue;
	public double minAngle;
	public double maxValue;
	public double maxAngle;
	public double r;
	public Point  c;
	
	
	void drawNeedle(
			Canvas canvas, double value, Paint paint)
	{	
		// define needle at zero degrees:
		float pts[] = {0.0f, 0.0f, (float)r, (float)r};

		
		double m = - (maxAngle - minAngle) / (maxValue - minValue);
		
		//   theta(minValue) = m * minValue + b = minAngle  
		double b = minAngle - m * minValue;		
		double angle_radians = m * value + b;
		
		// now rotate:
		boolean isX = true;
		for (int i = 0; i < pts.length; i++)
		{
			if (isX)
				pts[i] = (float)((double)pts[i] * Math.cos(angle_radians));
			else
				pts[i] = (float)((double)pts[i] * Math.sin(angle_radians));
			isX = !isX;
		}		
		
		// now translate:
		isX = true;
		for (int i = 0; i < pts.length; i++)
		{
			if (isX)
				pts[i] = pts[i] + c.x;
			else
				pts[i] = pts[i] + c.y;
			isX = !isX;
		}		
		
		canvas.drawLines(pts, paint);

/*		 float needleX, needleY, needleHeight, needleWidth;


		 Paint linePaint;
		 Path linePath;

		needleX = 510;//this.getWidth()/2;
		needleY = 600;//this.getHeight()/3;
		needleHeight = 400;
		needleWidth = 5;

		linePaint = new Paint();
		linePaint.setColor(Color.RED); // Set the color
		linePaint.setStyle(Paint.Style.FILL_AND_STROKE); // set the border and fills the inside of needle
		linePaint.setAntiAlias(true);
		linePaint.setStrokeWidth(2.0f); // width of the border
		//linePaint.setShadowLayer(8.0f, 0.1f, 0.1f, Color.GRAY); // Shadow of the needle

		linePath = new Path();
		linePath.moveTo(needleX, needleY);
		linePath.lineTo(needleX+20.0f, needleY-needleWidth);
		linePath.lineTo(needleX+needleHeight,needleY);
		linePath.lineTo(needleX+20.0f, needleY+needleWidth);
		linePath.lineTo(needleX,needleY);
		linePath.addCircle(needleX+40.0f, needleY, 10.0f, Path.Direction.CW);
		linePath.close();

		canvas.drawPath(linePath, linePaint);*/
		
	}

}
