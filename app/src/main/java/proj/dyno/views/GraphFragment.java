package proj.dyno.views;


import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import proj.dyno.R;
import proj.dyno.objects.Dashboard;
import proj.dyno.objects.GaugeView;


/**
 * A simple {@link Fragment} subclass.
 */
public class GraphFragment extends Fragment implements OnChartValueSelectedListener {

  private LineChart mChart;
  private Button btnStart;
  private GaugeView gaugeView;
  LineDataSet setHP, setMPH, setTorque;
  LineData lineData;
  int indexChart = 0;
  int fragType;

  LinearLayout torqueView;

  TextView tvTorque, tvTorqueMax, tvTorqueMaxRpm, tvHp, tvHpMax, tvHpMaxRpm;

  public GraphFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view =  inflater.inflate(R.layout.fragment_graph, container, false);


    torqueView = (LinearLayout) view.findViewById(R.id.torque_values);
    gaugeView = (GaugeView) view.findViewById(R.id.odbii);

    mChart = (LineChart) view.findViewById(R.id.chart1);
    mChart.setOnChartValueSelectedListener(this);
    Log.d("TONS", "start setup chart ");

    // no description text
    mChart.setDescription("");
    mChart.setNoDataTextDescription("You need to provide data for the chart.");
    mChart.setScaleEnabled(false);
    mChart.setHighlightPerDragEnabled(false);
    mChart.setDrawGridBackground(false);
    mChart.getAxisRight().setEnabled(false);

    mChart.getXAxis().setEnabled(false);
/*    mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    mChart.getXAxis().setDrawAxisLine(false);
    mChart.getXAxis().setDrawGridLines(false);*/

    mChart.getAxisLeft().setEnabled(true);
    mChart.getAxisLeft().setTextColor(Color.WHITE);
    mChart.getAxisLeft().setLabelCount(3, true);
    mChart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
    mChart.getAxisLeft().setDrawAxisLine(false);
    mChart.getAxisLeft().setDrawGridLines(false);
    mChart.getLegend().setTextColor(Color.WHITE);
    mChart.getLegend().setEnabled(true);
    mChart.setBackgroundColor(Color.BLACK);
    mChart.setMaxVisibleValueCount(25);

    Log.d("TONS", "setup chart finished");
    setData();
    setFragType(fragType);

    gaugeView.pauseThread();
    btnStart = (Button) view.findViewById(R.id.btn_start);
    btnStart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        gaugeView.startThread();

        GaugeView.GaugeThread gaugeThread = gaugeView.getThread();

        float mult = 30 / 2f;
        float mph, torque, hp;
        mph = (float) (Math.random() * mult) + 50;
        torque = (float) (Math.random() * mult) + 150;
        hp = (float) (Math.random() * mult) + 200;
        addData(mph, torque, hp);
        gaugeThread.setMPH(mph);
        gaugeThread.setRPM(hp);


      }
    });

    tvTorque = (TextView)view.findViewById(R.id.tv_torque);
    tvTorqueMax = (TextView)view.findViewById(R.id.tv_torque_max);
    tvTorqueMaxRpm = (TextView)view.findViewById(R.id.tv_torque_max_rpm);
    tvHp = (TextView)view.findViewById(R.id.tv_hp);
    tvHpMax = (TextView)view.findViewById(R.id.tv_hp_max);
    tvHpMaxRpm = (TextView)view.findViewById(R.id.tv_hp_max_rpm);


    return view;

  }

  public void setFragType(int fragType){
    this.fragType = fragType;

    if(gaugeView != null) {
      gaugeView.getThread().setFragType(fragType);

      if(fragType == Dashboard.FRAG_GRAPH){
        torqueView.setVisibility(View.GONE);
        //gaugeView.setVisibility(View.VISIBLE);
        //setData();
        //gaugeView.invalidate();

      }else{
        torqueView.setVisibility(View.VISIBLE);
        //gaugeView.setVisibility(View.GONE);

      }
      gaugeView.invalidate();
    }
  }

  private void addData(float mph, float torque, float hp){
    lineData.addXValue(indexChart+"");

    Log.d("TONS", setMPH.getEntryCount() + "");

    setMPH.addEntry(new Entry(mph, indexChart));
    setTorque.addEntry(new Entry(torque,indexChart));
    setHP.addEntry(new Entry(hp, indexChart));

    if(setMPH.getEntryCount() == 30) {
      setMPH.removeEntry(0);
      setTorque.removeEntry(0);
      setHP.removeEntry(0);
      lineData.removeXValue(0);
      for(int i = 0; i < setMPH.getYVals().size(); i++){
        setMPH.getYVals().get(i).setXIndex(i);
        setTorque.getYVals().get(i).setXIndex(i);
        setHP.getYVals().get(i).setXIndex(i);
      }
    }

    setMPH.notifyDataSetChanged();
    setTorque.notifyDataSetChanged();
    setHP.notifyDataSetChanged();

    lineData.notifyDataChanged();

    mChart.notifyDataSetChanged(); // let the chart know it's data changed
    mChart.invalidate(); // refresh

    //mChart.setVisibleXRangeMaximum(20);

    indexChart++;
  }


  private void setData() {

    int count = 1;

    ArrayList<String> xVals = new ArrayList<String>();
    for (int i = 0; i < count; i++) {
      xVals.add((i) + "");
    }

    // create a dataset and give it a type
    setHP = new LineDataSet(new ArrayList<Entry>(), "MPH");
    setHP.setAxisDependency(YAxis.AxisDependency.LEFT);
    setHP.setColor(Color.YELLOW);
    setHP.setDrawCircles(false);
    setHP.setLineWidth(2f);
    setHP.setFillAlpha(65);
    setHP.setDrawValues(false);
    setHP.setDrawCubic(false);

    // create a dataset and give it a type
    setTorque = new LineDataSet(new ArrayList<Entry>(), "TORQUE");
    setTorque.setAxisDependency(YAxis.AxisDependency.RIGHT);
    setTorque.setDrawCircles(false);
    setTorque.setColor(ColorTemplate.getHoloBlue()) ;
    setTorque.setLineWidth(2f);
    setTorque.setFillAlpha(65);
    setTorque.setDrawValues(false);
    setTorque.setDrawCubic(false);


    setMPH = new LineDataSet(new ArrayList<Entry>(), "HP");
    setMPH.setAxisDependency(YAxis.AxisDependency.RIGHT);
    setMPH.setDrawCircles(false);
    setMPH.setColor(Color.RED) ;
    setMPH.setLineWidth(2f);
    setMPH.setFillAlpha(65);
    setMPH.setDrawValues(false);
    setMPH.setDrawCubic(false);

    ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
    dataSets.add(setMPH);
    dataSets.add(setTorque);
    dataSets.add(setHP); // add the datasets

    // create a data object with the datasets
    lineData = new LineData(xVals, dataSets);
    lineData.setValueTextColor(Color.WHITE);
    lineData.setValueTextSize(9f);

    // set data
    mChart.setData(lineData);
  }

  @Override
  public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

  }

  @Override
  public void onNothingSelected() {

  }
}
