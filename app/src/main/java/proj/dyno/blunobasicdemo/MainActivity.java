package proj.dyno.blunobasicdemo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import proj.dyno.R;

public class MainActivity extends BlunoLibrary {
    private Button buttonScan;
    private Button buttonSerialSend;
    private EditText serialSendText;
    private TextView serialReceivedText;
    private StringBuffer mStringBuffer;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView(R.layout.activity_main);
        onCreateProcess();                                                        //onCreate Process by BlunoLibrary

        serialBegin(115200);
        //buttonScanOnClickProcess();

       /* serialReceivedText = (TextView) findViewById( R.id.serialReveicedText );    //initial the EditText of the received data
        serialSendText = (EditText) findViewById( R.id.serialSendText );            //initial the EditText of the sending data

        buttonSerialSend = (Button) findViewById( R.id.buttonSerialSend );        //initial the button for sending the data
        buttonSerialSend.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick( View v ) {
                // TODO Auto-generated method stub
                serialSend( "x" );               //send the data to the BLUNO
            }
        } );

        buttonScan = (Button) findViewById( R.id.buttonScan );                    //initial the button for scanning the BLE device
        buttonScan.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick( View v ) {
                // TODO Auto-generated method stub
                buttonScanOnClickProcess();                                        //Alert Dialog for selecting the BLE device
            }
        } );*/
    }

    protected void onResume() {
        super.onResume();
        System.out.println( "BlUNOActivity onResume" );
        onResumeProcess();                                                        //onResume Process by BlunoLibrary
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        onActivityResultProcess( requestCode, resultCode, data );                    //onActivityResult Process by BlunoLibrary
        super.onActivityResult( requestCode, resultCode, data );
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPauseProcess();                                                        //onPause Process by BlunoLibrary
    }

    protected void onStop() {
        super.onStop();
        onStopProcess();                                                        //onStop Process by BlunoLibrary
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDestroyProcess();                                                        //onDestroy Process by BlunoLibrary
    }

    @Override
    public void onConectionStateChange(
            connectionStateEnum theConnectionState ) {//Once connection state changes, this function will be called
        switch( theConnectionState ) {                                            //Four connection state
            case isConnected:
                buttonScan.setText( "Connected" );
                break;
            case isConnecting:
                buttonScan.setText( "Connecting" );
                break;
            case isToScan:
                buttonScan.setText( "Scan" );
                break;
            case isScanning:
                buttonScan.setText( "Scanning" );
                break;
            case isDisconnecting:
                buttonScan.setText( "isDisconnecting" );
                break;
            default:
                break;
        }
    }

    @Override
    public void onSerialReceived( String bleData ) {
//        if( mStringBuffer == null ) {
//            mStringBuffer = new StringBuffer();
//        }
//        mStringBuffer.append( bleData );
//        if( bleData.contains( "@" ) ) {
//            String data = mStringBuffer.toString();
//            data = data.replace( "(\\r|\\n)", "" );
//            String dataArr[] = data.split( "," );
//            for( int i = 1; i < dataArr.length - 1; i++ ) {
//                Log.e( "Bluno", dataArr[ i ] );
//            }
//            mStringBuffer = null;
//        }
    }
}